const fs = require('fs');
const url = require('url');

var urls = process.argv.slice(2);

var data = JSON.parse(fs.readFileSync('apps.json'));

function builtwith(url, headers = null, html = null, userAgent = 'builtwith')
{
    techs = {};

    for(index in data['apps'])
    {
        if('url' in data['apps'][index])
        {
            if(contains(url, data['apps'][index]['url']))
            {
                add_app(techs, index, data['apps'][index]);
            }
        }
    }
}

function contains(v, regex)
{
        // return re.compile(regex.split('\\;')[0], flags=re.IGNORECASE).search(v)
    var regex = new RegExp(regex.split('\\;')[0], 'i');
    return regex[Symbol.search](v);
}

// def get_categories(app_spec):
//     """Return category names for this app_spec
//     """
//     return [data['categories'][str(c_id)] for c_id in app_spec['cats']]

function getCategories(appSpec)
{
    cats = [];

    for(c_id in appSpec['cats'])
    {
        cats.push(data['categories'][c_id]);
    }

}

// def add_app(techs, app_name, app_spec):
//     """Add this app to technology
//     """
//     for category in get_categories(app_spec):
//         if category not in techs:
//             techs[category] = []
//         if app_name not in techs[category]:
//             techs[category].append(app_name)
//             implies = app_spec.get('implies', [])
//             if not isinstance(implies, list):
//                 implies = [implies]
//             for app_name in implies:
//                 add_app(techs, app_name, data['apps'][app_name])

function add_app(techs, appName, appSpec)
{
    for(category in getCategories(appSpec))
    {
        if(!(category in techs)
        {
            techs[category] = [];
        }
        if(!(appName in techs[category]))
        {
            techs[category].push(appName);
            
        }
    }
}

builtwith('matmunn.me');